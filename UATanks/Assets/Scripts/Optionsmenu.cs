﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Optionsmenu : MonoBehaviour {
	public Transform canvas;
	public Transform kansas;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
		
	public void Restartbtn()
	{
		SceneManager.LoadScene ("BattleoftheBards");
	}

	public void qutbtn()
	{
		Application.Quit ();
	}

	public void mainMenubtn()
	{
		SceneManager.LoadScene ("Mainmenu");
	}

	public void combobtn()
	{
		if (canvas.gameObject.activeInHierarchy == false) 
		{
			canvas.gameObject.SetActive (true);
			}
		else {
			canvas.gameObject.SetActive (false);
		}
	}

	public void controlbtn()
	{
		if (kansas.gameObject.activeInHierarchy == false) 
		{
			kansas.gameObject.SetActive (true);
		}
		else {
			kansas.gameObject.SetActive (false);
		}
	}
}
