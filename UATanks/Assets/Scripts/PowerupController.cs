﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour {

	public TankData data;
	public List<Powerup> powerupList;

	// Use this for initialization
	void Start () {
		data = GetComponent<TankData> ();
	}
	
	// Update is called once per frame
	void Update () {
		// TODO: Cheat to add a powerup for testing -- DELETE ME!
		if (Input.GetKeyDown (KeyCode.E)) {
			Powerup temp = new Powerup ();
			temp.duration = 10;
			AddPowerup (temp);
		}

		// Update our powerups
		UpdateAllPowerups ();
	}

	public void UpdateAllPowerups () {
		// Create a list to hold the ones we want to remove
		List<Powerup> powerUpsToRemove = new List<Powerup>();

		foreach (Powerup pu in powerupList) {
			// Update the powerup
			pu.OnUpdate (data);
			// If the powerup is ready to be removed, add to a list of powerupsToRemove
			if (pu.timeRemaining <= 0) {
				powerUpsToRemove.Add(pu);
			}
		}

		// Now that I am done with my foreach, I can remove the powerups in the list
		foreach (Powerup pu in powerUpsToRemove) {
			RemovePowerup (pu);
		}

		
	}

	public void AddPowerup ( Powerup powerUpToAdd ) {
		// Add the powerup to the list
		powerupList.Add(powerUpToAdd);

		// Run the OnActivate method from our powerup
		powerUpToAdd.OnActivate(data);
	}

	public void RemovePowerup (Powerup powerUpToRemove) {
		powerupList.Remove (powerUpToRemove);
		powerUpToRemove.OnDeactivate (data);
	}

}
