﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour {

	public GameObject prefabToSpawn;
	public float spawnTime;

	private GameObject spawnedObject;
	private float countdownRemaining;

	// Use this for initialization
	void Start () {
		countdownRemaining = spawnTime;
	}
	
	// Update is called once per frame
	void Update () {

		// Only process if there is no spawned object
		if (spawnedObject == null) {
			// Update my timer
			countdownRemaining -= Time.deltaTime;

			// If it is time to spawn, spawn and reset timer
			if (countdownRemaining <= 0) {
				spawnedObject = Instantiate (prefabToSpawn) as GameObject;
				countdownRemaining = spawnTime;
			}
		}

	}
}
