﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupTurn : Pickup
{

    // Use this for initialization
    // Override the variable "powerup" to be our PowerupHealth type
    public PowerupTurn powerup;
    AudioSource m_MyAudioSource;
    public void OnTriggerEnter(Collider other)
    {
        m_MyAudioSource = GetComponent<AudioSource>();
        //Play the AudioClip attached to the AudioSource on startup
        m_MyAudioSource.Play();
        // Get the powerup controller from whatever hit us
        PowerupController otherPC;
        otherPC = other.gameObject.GetComponent<PowerupController>();
        // If it exists (they have one)
        if (otherPC != null)
        {
            // Give them this powerup
            otherPC.AddPowerup(powerup);
            // Destroy this pickup
            Destroy(gameObject);
        }

    }
}
