﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PowerupHealth : Powerup {

	public float healthDelta;

	public override void OnActivate (TankData target) {
		// Run OnActivate from parent class
		base.OnActivate(target);

		// Increase health by healthDelta
		if (target.health != null) {
			target.health.health += healthDelta;
		}
	}

	public override void OnDeactivate (TankData target) {
		// Run from parent
		base.OnDeactivate(target);

		// Decrease health by healthDelta		
		if (target.health != null) {
			target.health.health -= healthDelta;
		}
	}

	public override void OnUpdate (TankData target) {
		// Run from parent
		base.OnUpdate(target);
	}
}
