﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	[Header("Components")]
	public TankData data;

	[Header("Inputs")]
	public KeyCode forwardKey;
	public KeyCode backwardKey;
	public KeyCode turnRightKey;
	public KeyCode turnLeftKey;
	public KeyCode shootKey;

	// Use this for initialization
	void Start () {
		GameManager.instance.player = this;
	}

	// Use this for destruction
	void OnDestroy () {
		GameManager.instance.player = null;
	}

	
	// Update is called once per frame
	void Update () {

		// Handle Movement
		float moveSpeed = 0.0f;
		if (Input.GetKey (forwardKey)) {
			moveSpeed = data.moveForwardSpeed;
		}
		if (Input.GetKey (backwardKey)) {
			// TODO: Setup Move backward
		}
		data.gameObject.SendMessage ("Move", moveSpeed);

		// Handle turning
		if (Input.GetKey (turnLeftKey)) {
			data.gameObject.SendMessage ("Turn", -1 * data.turnSpeed);
		}
		if (Input.GetKey (turnRightKey)) {
			data.gameObject.SendMessage ("Turn", data.turnSpeed);
		}

		// Handle Shooting
		if (Input.GetKey (shootKey)) {
			data.gameObject.SendMessage ("Shoot", SendMessageOptions.DontRequireReceiver);
		}

	}
}
