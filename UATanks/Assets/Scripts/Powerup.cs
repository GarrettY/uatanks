﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Powerup {

	public float duration;
	[HideInInspector] public float timeRemaining;


	public virtual void OnActivate (TankData target) {
		// Set the timer
		timeRemaining = duration;
	}

	public virtual void OnDeactivate (TankData target) {
	}

	public virtual void OnUpdate (TankData target) {
		// Anything we do every frame draw goes here!
		timeRemaining -= Time.deltaTime;
	}

}
