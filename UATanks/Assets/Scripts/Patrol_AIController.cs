﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol_AIController : MonoBehaviour {

 	public TankData data;
	public List<Transform> waypoints;
	public float closeEnoughDistance;
	public float fleeDistance;
    
	public float obsAvoidDistance;
	public float avoidMoveTime;
	public enum AvoidanceStates {None, TurnToAvoid, MoveToAvoid};
	public AvoidanceStates avoidState;
	public LayerMask obstacleLayers;
	public float timeEnteredAvoidState;
	public enum AIStates {Start, Idle, Patrol, FleeFromPlayer, ChasePlayer, SelfDestruct};
	public AIStates AIState;
	public float timeEnteredAIState;

	public bool isActive = true;

	[Header("Waypoints")]
	public int currentWaypoint;
	public PatrolTypes patrolType;

	// Private variables
	private int patrolDirection = 1; // 1 is forward, -1 is backward through waypoints


	// Use this for initialization
	void Start () {
		data = GetComponent<TankData> ();
		ChangeState (AvoidanceStates.None);
		ChangeState (AIStates.Start);
        
    }
	
	// Update is called once per frame
	void Update () {
		if (!isActive)
			return;

		Transform playerTf = GameManager.instance.player.data.tf;

		switch (AIState) {
		case AIStates.Start:
			// Do Work
			// Check for Transitions
			ChangeState(AIStates.Patrol);
			break;
		case AIStates.Patrol:
			// Do Work
			Patrol ();
			// Check for Transitions
			if (Input.GetKeyDown(KeyCode.C)) {
				ChangeState (AIStates.ChasePlayer);
			}
                if (Input.GetKeyDown(KeyCode.R))
                {
                    ChangeState(AIStates.FleeFromPlayer);
                }
                break;
		case AIStates.ChasePlayer:
                // Do Work
                // Check for Transitions
                Chase(playerTf);
                if (Input.GetKeyDown(KeyCode.Z))
                {
                    ChangeState(AIStates.Patrol);
                }
                break;
		case AIStates.Idle:
                if (Input.GetKeyDown(KeyCode.G))
                {
                    ChangeState(AIStates.ChasePlayer);
                }
                // Do Work
                // Check for Transitions
                break;
		case AIStates.FleeFromPlayer:
                // Do Work
                // Check for Transitions
                Flee(playerTf);

                if (Input.GetKeyDown(KeyCode.T))
                {
                    ChangeState(AIStates.Patrol);
                }
                break;
		case AIStates.SelfDestruct:
			// Do Work
			// Check for Transitions
			break;
		}


	}

	void Chase ( Transform target ) {


        switch (avoidState)
        {

            case AvoidanceStates.None:
                // Do Work
                //If we're not at the waypoint


                // Rotate towards player

                // Create a vector from our position to target
                //       (targetPoint - startPoint)
                Vector3 vectorToTarget = target.position - data.mover.tf.position;

                // Create a set of rules (Quaternion) that will point our tank down the vector to target.
                Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget, Vector3.up);

                data.tf.rotation = Quaternion.RotateTowards(data.tf.rotation,
                                                                targetRotation,
                                                                data.turnSpeed * Time.deltaTime);

                // Move Forward
                data.mover.Move(data.moveForwardSpeed);

                // Check for transitions
                if (!CanMoveForward(obsAvoidDistance))
                {
                    ChangeState(AvoidanceStates.TurnToAvoid);
                }
                break;

            case AvoidanceStates.MoveToAvoid:
                // Do work
                data.mover.Move(data.moveForwardSpeed);
                // Check for transitions
                if (Time.time - timeEnteredAvoidState > avoidMoveTime)
                {
                    ChangeState(AvoidanceStates.None);
                }
                if (!CanMoveForward(obsAvoidDistance))
                {
                    ChangeState(AvoidanceStates.TurnToAvoid);
                }
                break;

            case AvoidanceStates.TurnToAvoid:
                // Do work
                data.mover.Turn(data.turnSpeed);
                // Check for Transitions
                if (CanMoveForward(obsAvoidDistance))
                {
                    ChangeState(AvoidanceStates.MoveToAvoid);
                }
                break;
        }
    }


	void Flee ( Transform target ) {

        switch (avoidState)
        {

            case AvoidanceStates.None:
                // Do Work
                //If we're not at the waypoint

                // Find vector to target
                Vector3 vectorToTarget = (target.position - data.tf.position);

                // Flip it to reverse direction
                vectorToTarget = -1 * vectorToTarget;

                // Resize it to my fleeDistance
                vectorToTarget = vectorToTarget.normalized * fleeDistance;

                // Find the point at the end of that vector from my position
                Vector3 fleeTarget = data.tf.position + vectorToTarget;

                // Move to my new target point
                // Create a vector from our position to target
                //       (targetPoint - startPoint)
                vectorToTarget = fleeTarget - data.mover.tf.position;

                // Create a set of rules (Quaternion) that will point our tank down the vector to target.
                Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget, Vector3.up);

                data.tf.rotation = Quaternion.RotateTowards(data.tf.rotation,
                    targetRotation,
                    data.turnSpeed * Time.deltaTime);

                // Move Forward
                data.mover.Move(data.moveForwardSpeed);

                // Check for transitions
                if (!CanMoveForward(obsAvoidDistance))
                {
                    ChangeState(AvoidanceStates.TurnToAvoid);
                }
                break;

            case AvoidanceStates.MoveToAvoid:
                // Do work
                data.mover.Move(data.moveForwardSpeed);
                // Check for transitions
                if (Time.time - timeEnteredAvoidState > avoidMoveTime)
                {
                    ChangeState(AvoidanceStates.None);
                }
                if (!CanMoveForward(obsAvoidDistance))
                {
                    ChangeState(AvoidanceStates.TurnToAvoid);
                }
                break;

            case AvoidanceStates.TurnToAvoid:
                // Do work
                data.mover.Turn(data.turnSpeed);
                // Check for Transitions
                if (CanMoveForward(obsAvoidDistance))
                {
                    ChangeState(AvoidanceStates.MoveToAvoid);
                }
                break;
        }

    }

	void AIMoveToPoint (Vector3 Point) {

	}


	void Patrol () {



        switch (avoidState) {

		case AvoidanceStates.None:
			// Do Work
			//If we're not at the waypoint
			if (Vector3.Distance (data.mover.tf.position, waypoints [currentWaypoint].position) > closeEnoughDistance) {

				// Move to waypoint
				float moveSpeed = 0.0f;

				// Create a vector from our position to target
				//       (targetPoint - startPoint)
				Vector3 vectorToTarget = waypoints [currentWaypoint].position - data.mover.tf.position;

				// Create a set of rules (Quaternion) that will point our tank down the vector to target.
				Quaternion targetRotation = Quaternion.LookRotation (vectorToTarget, Vector3.up);

				// If we are NOT facing our goal
				if (data.mover.tf.rotation != targetRotation) {
					// Turn to face the goal

					// Change our rotation rules (no more than our Turnspeed allows) towards that set of rules
					data.mover.tf.rotation = Quaternion.RotateTowards (data.mover.tf.rotation, 
						targetRotation, 
						data.turnSpeed * Time.deltaTime); 
				} else {
					// If we ARE facing our goal, move forward
					moveSpeed = data.moveForwardSpeed;
				}

				// Actually move forward every frame (so gravity is applied, even if movement is 0);
				data.mover.Move (moveSpeed);
			} else {
				// We are the at the waypoint... 
				if (patrolType == PatrolTypes.Loop) {
					currentWaypoint++;
					if (currentWaypoint >= waypoints.Count) {
						currentWaypoint = 0;
					}
				} else if (patrolType == PatrolTypes.Stop) {
					currentWaypoint++;
					if (currentWaypoint >= waypoints.Count) {
						isActive = false;
					}
				} else if (patrolType == PatrolTypes.PingPong) {
					ChangePatrolDirection ();			
				}
			}
			// Check for transitions
			if (!CanMoveForward (obsAvoidDistance)) {
				ChangeState(AvoidanceStates.TurnToAvoid);
			}
			break;

		case AvoidanceStates.MoveToAvoid:
			// Do work
			data.mover.Move (data.moveForwardSpeed);
			// Check for transitions
			if (Time.time - timeEnteredAvoidState > avoidMoveTime) {
				ChangeState (AvoidanceStates.None);
			}
			if (!CanMoveForward (obsAvoidDistance)) {
				ChangeState (AvoidanceStates.TurnToAvoid);
			}
			break;

		case AvoidanceStates.TurnToAvoid:
			// Do work
			data.mover.Turn (data.turnSpeed);
			// Check for Transitions
			if (CanMoveForward (obsAvoidDistance)) {
				ChangeState (AvoidanceStates.MoveToAvoid);
			}
			break;
		}
	}

	void ChangeState ( AIStates newState ) {
		AIState = newState;
		timeEnteredAIState = Time.time;
	}


	void ChangeState ( AvoidanceStates newState ) {
		Debug.Log ("Changing to state: " + newState.ToString());
		// change the state
		avoidState = newState;
		// Keep track of the time we entered this state
		timeEnteredAvoidState = Time.time;
	}


	private void ChangePatrolDirection () {

		/*************** One method **************
				currentWaypoint += patrolDirection;
				if (currentWaypoint >= waypoints.Count) {
					patrolDirection = -1; // Now go backwards
					currentWaypoint = waypoints.Count - 2;
				} else if (currentWaypoint < 0) { 
					patrolDirection = 1; // Now go forwards
					currentWaypoint = 1;
				}
				****************************************/

		// Change the direction we patrol in and set our waypoint appropriately
		currentWaypoint += patrolDirection;
		if (currentWaypoint >= waypoints.Count || currentWaypoint < 0) {
			patrolDirection *= -1;
			currentWaypoint += 2 * patrolDirection; 
		}

	}

	bool CanMoveForward ( float distance ) {

		RaycastHit hitInfo;
		if (Physics.Raycast(data.tf.position, data.tf.forward, out hitInfo,
			distance, obstacleLayers)) {
			return false;
		} else {
			return true;
		}
		
	}


	bool CanSee (GameObject target) {
		// TODO: Return true if we can see the target, return false if not
		return false;
	}

	bool CanHear (GameObject target) {
		// TODO: Return true if we can hear the target, return false if not
		return false;
	}

	bool isHealthBelow (float value) {
		if (data.health.health < value) {
			return true;
		} else {
			return false;
		}
	}

	bool isHealthAbove (float value) {
		if (data.health.health > value) {
			return true;
		} else {
			return false;
		}
	}


}
