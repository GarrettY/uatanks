﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShoot : MonoBehaviour
{
    public float force;
    public GameObject projectile;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine("Attack");
            //yield return new WaitForSeconds(1f);
        }
    }

    IEnumerator Attack() {


        yield return new WaitForSeconds(1f);
        GameObject bullet = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;
    bullet.GetComponent<Rigidbody>().AddForce(transform.forward* force);
        yield return new WaitForSeconds(3f);
        Destroy(bullet);
}
}
