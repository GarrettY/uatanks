﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerTests : MonoBehaviour {

	[Tooltip("Number of Seconds between Outputs")] public float timerDelay; 

	private DateTime timeVariable;

	// "Are We There Yet?" Method
	private float timerStart;

	//  Method 2
	private float timerEnd;

	// Countdown Method
	private float countdown;

	// Use this for initialization
	void Start () {
		timeVariable = DateTime.Now;

		// Method 1
		timerStart = Time.time;
		// Method 2
		timerEnd = Time.time + timerDelay;
		// Method 3
		countdown = timerDelay;
	}
	
	// Update is called once per frame
	void Update () {

		// "Are we there yet" method
		float timerExecuteTime = timerStart + timerDelay;
		if (Time.time >= timerExecuteTime ) {
			// Time to fire the timer
			Debug.Log("TIMER HAS FIRED!" + Time.time);
			// Save our current time as the new start time
			timerStart = Time.time;
		}

		// Method 2
		if (Time.time >= timerEnd) {
			Debug.Log ("Timer2 has fired" + Time.time);
			// Reset endtime
			timerEnd = Time.time + timerDelay;
		}

		// Method 3
		countdown -= Time.deltaTime;
		if (countdown <= 0) {
			Debug.Log ("Timer3 has fired" + Time.time);
			// reset our countdown
			countdown = timerDelay;
		}
	}
}
