﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PowerupTurn : Powerup
{


    public float TurnDelta;

    public override void OnActivate(TankData target)
    {
        // Run OnActivate from parent class
        base.OnActivate(target);

        // Increase speed by healthDelta
        if (target.health != null)
        {
            target.turnSpeed += TurnDelta;
        }
    }

    public override void OnDeactivate(TankData target)
    {
        // Run from parent
        base.OnDeactivate(target);

        // Decrease health by healthDelta		
        if (target.health != null)
        {
            target.turnSpeed -= TurnDelta;
        }
    }

    public override void OnUpdate(TankData target)
    {
        // Run from parent
        base.OnUpdate(target);
    }
}
