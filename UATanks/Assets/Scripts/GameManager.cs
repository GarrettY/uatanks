﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour {
    AudioSource m_MyAudioSource;
    public static GameManager instance;

	// Important Game Objects
	public PlayerController player;

	// Use this for initialization
	void Awake () {
		if (instance != null) {
			Destroy (gameObject);
		} else {
			instance = this;
			DontDestroyOnLoad (gameObject);
		}
	}

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.P))
        {
            m_MyAudioSource = GetComponent<AudioSource>();
            
            m_MyAudioSource.Play();
            
            SceneManager.LoadScene("Losescreen");
        }
	}
}
