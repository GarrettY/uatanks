﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class LevelBuilder : MonoBehaviour {
    public bool mapOfTheDay;
	public int numRows;
	public int numCols;
	public float tileWidth = 50.0f;
	public float tileHeight = 50.0f;
	public GameObject[] roomTiles;
	public Room[,] grid;
    public GameObject player;
	//public int seed;


	// Use this for initialization
	void Start () {

        Instantiate(player, new Vector3(-12, 30, -27), Quaternion.identity);
		BuildLevel ();		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void BuildLevel () {

		// Set our seed for our Random to Map of the Day
		int seed;

		// Set our seed based on our date
		DateTime currentTime;
		currentTime = DateTime.Now;
		currentTime = currentTime.Date;
		seed = (int)currentTime.Ticks;

        if (mapOfTheDay == true)
        {
            UnityEngine.Random.InitState(seed);
        }
		// Prepare the grid array
		grid = new Room[numCols, numRows];

		// For each row in our grid
		for (int currentRow = 0; currentRow < numRows; currentRow++) { 
			// For each column in that row
			for (int currentCol = 0; currentCol < numCols; currentCol++) {
				// Instantiate a random tile
				GameObject tempTile = Instantiate(RandomTile()) as GameObject;
				// Set the data for that tile 
				// Set its name to include its grid (x,y) position
				tempTile.name = "Tile("+currentCol+","+currentRow+")";
				// Set its parent
				Transform tempTileTf = tempTile.GetComponent<Transform>();
				tempTileTf.parent = this.gameObject.GetComponent<Transform>();
				// Set its position (and rotation?)
				Vector3 newPosition;
				newPosition.x = currentCol * tileWidth;
				newPosition.y = 0.0f;
				newPosition.z = currentRow * tileHeight;						
				tempTileTf.localPosition = newPosition;

				// Add the Room component of the tile to our grid (data)
				grid[currentCol, currentRow] = tempTile.GetComponent<Room>();
			}
		}

		// For each item in our grid
		// Set neighbors
		for (int x = 0; x < numCols; x++) {
			for (int y = 0; y < numRows; y++) {

				if (x == 0) {
					grid [x, y].westRoom = null;			
				} else
				{
					grid [x, y].westRoom = grid [x - 1, y];
				}

				if (x == numCols - 1) {
					grid [x, y].eastRoom = null;
				} else {
					grid [x, y].eastRoom = grid [x + 1, y];					
				}

				if (y == 0) {
					grid [x, y].southRoom = null;
				} else {
					grid [x, y].southRoom = grid[x, y-1];
				}

				if (y == numRows - 1) {
					grid [x, y].northRoom = null;
				} else {
					grid [x, y].northRoom = grid [x, y + 1];
				}

			}
		}

		// Create Maze
		OpenMaze();
	}

	public void OpenMaze () {
		// Create a stack of visited rooms (so we can backtrack through it later)
		Stack<Room> visitedRooms = new Stack<Room>();

		// Start with a random room from the grid, add it to the stack and mark as visited
		visitedRooms.Push(grid[UnityEngine.Random.Range(0,numCols), UnityEngine.Random.Range(0,numRows)]);

		// Start by looking at our only room on the stack
		Room currentRoom = visitedRooms.Peek();

		// Mark our start room as visited
		currentRoom.isVisited = true;

		while (currentRoom != null) {
			// Using the "top" item of the stack
			Room nextRoom = currentRoom.RandomUnvisitedNeighbor();

			// If no unvisted neighbors
			if (nextRoom == null) {
			// Move back in the stack
				visitedRooms.Pop();
			}
			else {
				
			// If there are unvisited neighbors, 
				// Open the doors 
				OpenDoors(currentRoom, nextRoom);

				// put it on top of the stack
				visitedRooms.Push(nextRoom);
				// Mark as visited
				nextRoom.isVisited = true;
			}

			// Repeat...
			// If the stack is empty, give me null, otherwise, advance to next room in stack
			if (visitedRooms.Count == 0) {
				currentRoom = null; 
			} else {
				currentRoom = visitedRooms.Peek ();
			}
		}
	}

	public void OpenDoors ( Room currentRoom, Room nextRoom ) {

		if (currentRoom.northRoom == nextRoom) {
			currentRoom.northWall.SetActive (false);
			nextRoom.southWall.SetActive (false);
		}

		if (currentRoom.southRoom == nextRoom) {
			currentRoom.southWall.SetActive (false);
			nextRoom.northWall.SetActive (false);
		}

		if (currentRoom.eastRoom == nextRoom) {
			currentRoom.eastWall.SetActive (false);
			nextRoom.westWall.SetActive (false);
		}

		if (currentRoom.westRoom == nextRoom) {
			currentRoom.westWall.SetActive (false);
			nextRoom.eastWall.SetActive (false);
		}

	}


	public GameObject RandomTile ( ) {
		// Return a random tile from our list
		return roomTiles[UnityEngine.Random.Range(0, roomTiles.Length)];
	}

}


