﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]

public class TankMover : MonoBehaviour {

	public Transform tf;
	private CharacterController cc;

	// Use this for initialization
	void Start () {

		// Load my components	
		tf = GetComponent<Transform> ();
		cc = GetComponent<CharacterController> ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void Turn (float turnSpeedAndDirection) {
		tf.Rotate(new Vector3 (0, turnSpeedAndDirection * Time.deltaTime, 0));
	}

	public void Move ( float moveSpeedAndDirection ) {
		cc.SimpleMove ( tf.forward * moveSpeedAndDirection);
	}
		
}
