﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour {
	public GameObject PauseUI;
	private bool paused=false;
	//public transform canvas
	// Use this for initialization
	void Start () {
		PauseUI.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Pause")) {
			paused = !paused;
		}

		if (paused) {
			PauseUI.SetActive (true);
			Time.timeScale = 0;
		}

		if (!paused) {
			PauseUI.SetActive (false);
			Time.timeScale = 1;
		}
		//if (Input.GetKeyDown (KeyCode.Escape)) 
		//{
			//resume ();

		//}
	}

	//public void resume()
	//{
	//if (canvas.gameObject.activeInHierarchy == false) 
	//{
	//	canvas.gameObject.SetActive (true);
	//	Time.timeScale = 0;
	//	}
	//else {
	//	canvas.gameObject.SetActive (false);
	//	Time.timeScale = 1;
	//}
}
