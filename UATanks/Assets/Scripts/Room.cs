﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour {

	public GameObject northWall;
	public GameObject southWall;
	public GameObject eastWall;
	public GameObject westWall;

	public Room northRoom;
	public Room southRoom;
	public Room eastRoom;
	public Room westRoom;

	public bool isVisited = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public Room RandomUnvisitedNeighbor () {
		// Put all the unvisited neighbors in a list
		List<Room> unvisitedNeighbors = new List<Room>();
		if (northRoom != null && !northRoom.isVisited)
			unvisitedNeighbors.Add (northRoom);
		if (southRoom != null && !southRoom.isVisited)
			unvisitedNeighbors.Add (southRoom);
		if (eastRoom != null && !eastRoom.isVisited)
			unvisitedNeighbors.Add (eastRoom);
		if (westRoom != null && !westRoom.isVisited)
			unvisitedNeighbors.Add (westRoom);

		if (unvisitedNeighbors.Count == 0) {
			// If all our neighbors are visited, return null
			return null;
		} else {
			// If there was at least one unvisited neighbor, return a random one from the list
			return unvisitedNeighbors [UnityEngine.Random.Range (0, unvisitedNeighbors.Count)];
		}
	}



}
