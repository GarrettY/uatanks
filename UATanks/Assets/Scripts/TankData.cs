﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour {

	[Header("Components")]
	public TankMover mover;
	public TankShooter shooter;
	public TankHealth health;
	public Transform tf;

	[Header ("Data")]
	public float turnSpeed;
	public float moveForwardSpeed;

	// Use this for initialization
	void Start () {
		mover = GetComponent<TankMover> ();
		shooter = GetComponent<TankShooter> ();
		health = GetComponent<TankHealth> ();
		tf = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
